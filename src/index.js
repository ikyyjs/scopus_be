const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const homeRouter = require('./pages/home');
const scopusRouter = require('./pages/scopus');

const app = express();
const port = 3000;

app.use(cors());
app.use(bodyParser.json());

app.use('/', homeRouter);
app.use('/scopus', scopusRouter);

app.listen(port, () => {
    console.log(`Server berjalan di http://localhost:${port}`);
});
