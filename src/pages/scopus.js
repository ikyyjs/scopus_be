const express = require("express");
const router = express.Router();
const connection = require("../db/mysql");

router.post("/", (req, res) => {
  console.log("POST /scopus");
  const receivedData = req.body;
  console.log("Data yang diterima dari frontend:", receivedData);

  const sql =
    "INSERT INTO ngab_table (title, creator, publicationName, year, volume) VALUES ?";
  const values = receivedData.map((item) => [
    item.title,
    item.creator,
    item.publicationName,
    item.year,
    item.volume,
  ]);

  connection.query(sql, [values], (error, results, fields) => {
    if (error) {
      console.error("Error saat menyimpan data ke MySQL:", error);
      res.status(500).json({ message: "Gagal menyimpan data ke MySQL" });
    } else {
      console.log("Data berhasil disimpan ke MySQL");
      res
        .status(200)
        .json({ message: "Data diterima dan disimpan dengan sukses!" });
    }
  });
});

router.get("/", (req, res) => {
  console.log("GET /scopus");
  res.send(`
    <html>
    <head>
        <title>Scopus</title>
        <style>
            body, html {
                height: 100%;
                margin: 0;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .centered {
                text-align: center;
            }
            .back-button {
                position: absolute;
                top: 30px;
                left: 30px;
                padding: 10px 20px;
                background-color: black;
                color: white;
                text-decoration: none;
                border: 1px solid black;
                transition: color 0.5s ease, background-color 0.5s ease, border-color 0.5s ease; /* Mengatur durasi transisi */
            }
            .back-button:hover {
                color: black;
                background-color: transparent;
                border-color: black;
            }
        </style>
    </head>
    <body>
        <a href="/" class="back-button">&lt; Back</a>
        <div class="centered">
            <h1>Halaman Scopus</h1>
            <p>Kamu dapat melihat data melalui terminal!</p>
        </div>
    </body>
    </html>
    `);
});

module.exports = router;
