const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    console.log('GET /home');
    res.send(`
    <html>
    <head>
        <title>Home</title>
        <style>
            body, html {
                height: 100%;
                margin: 0;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .centered {
                text-align: center;
            }
            h1 {
                margin-bottom: 20px;
            }
            .button-link {
                display: inline-block;
                padding: 10px 20px;
                background-color: black;
                color: white;
                text-decoration: none;
                border: 1px solid black;
                transition: color 0.5s ease, background-color 0.5s ease, border-color 0.5s ease; /* Mengatur durasi transisi */
            }
            
            .button-link:hover {
                color: black;
                background-color: transparent;
                border-color: black;
            }
            
        </style>
    </head>
    <body>
        <div class="centered">
            <h1>ngabdion</h1>
            <a href="/scopus" class="button-link">Menuju ke /scopus</a>
        </div>
    </body>
    </html>
    `);
});

module.exports = router;
